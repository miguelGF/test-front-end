import React, { Component } from 'react';
import './App.css';

import NavBar from './components/NavBar/NavBar';
import SubBar from './components/SubBar/SubBar';
import Section1 from './components/Section1/Section1';
import Section2 from './components/Section2/Section2';
import Section3 from './components/Section3/Section3';
import Section4 from './components/Section4/Section4';
import Section5 from './components/Section5/Section5';
import Section6 from './components/Section6/Section6';
import Section7 from './components/Section7/Section7';
import Section8 from './components/Section8/Section8';
import Newsletter from './components/Newsletter/Newsletter';
import Footer from './components/Footer/Footer';


class App extends Component {
  
  render() {
    
    return (
      <div className="App">
        
        <div className="container">
          <NavBar />
          <SubBar />
        </div>

         <div className="section1 jumbotron jumbotron-fluid">
            <div className="container">
            <br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
              <h1><span className="banner">Estilo<br />Escandinavo</span></h1>
            </div>
        </div>

        <Section2 />
        <Section4 />
        <Section3 />
        <Section5 />
        
     
        <Section6 />
        <Section7 />
        <Section8 />

        <Newsletter />
        <Footer />
        
      </div>
    );
  }
  
}

export default App;
