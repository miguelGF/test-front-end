import React, { Component } from 'react';
import './Section7.css';

import section7 from './section7.png';

class Section7 extends Component {
	render() {
		return (
			<div className="section7 jumbotron jumbotron-fluid">
	          <div className="container" align="">
	              <div className="row justify-content-md-center">
	              	  <div className="col-sm-6 imagen">
	                    <img src={section7} className="img-fluid" alt="Responsive image"/>
	                  </div>
	                  <div className="col-sm-6 texto" align="left">
	                   
	                    <h2>
	                    <div className="tendencias">
	                    
	                      Nuevas tendencias en<br />
	                      diseño de espacios<br />
	                      colaborativos para el 2020
	                    
	                    </div>
	                    </h2>
	                    <div className="tendencias2">
	                    <p>
	                      Estamos viviendo en tiempos de cambio, es normal 
	                      entrar en una oficina y encontrar marcadas diferencias
	                      con lo que se vivía hace 30 años, las nuevas tendencias
	                      y formas de trabajo que han surgido en los últimos
	                      años han obligado a los profesionales del diseño de
	                      interiores a reinventarse
	                    </p>
	                    </div>
	                    <div align="right">
	                      <button type="button" className="btn btn-outline-light">SEGUIR LEYENDO ></button>
	                    </div>
	                  </div>
	                  <div className="col-sm-6 imagen2">
	                    <img src={section7} className="img-fluid" alt="Responsive image"/>
	                  </div>

	              </div>
	          </div>
	        </div>
		);
	}
}

export default Section7;