import React, { Component } from 'react';
import './Section6.css';

import section6 from './section6.png';
import logo from './sincrology-logo.png';
import image1 from './section6-01.png';
import image2 from './section6-02.png';
import image3 from './section6-03.png';
import image4 from './section6-04.png';

class Section6 extends Component {
	render() {
		return (
      <div className="section6">
          <div className="container">
              <div className="row slider">
               
                  <div id="galeria" className="carousel slide carousel-fade" data-ride="carousel">
                    <div className="carousel-inner">
                      <div className="carousel-item active">
                        <img src={image1} className="d-block w-100 img-fluid" alt="Responsive image"/>
                      </div>
                      <div className="carousel-item">
                        <img src={image2} className="d-block w-100 img-fluid" alt="Responsive image"/>
                      </div>
                      <div className="carousel-item">
                        <img src={image3} className="d-block w-100 img-fluid" alt="Responsive image"/>
                      </div>
                      <div className="carousel-item">
                        <img src={image4} className="d-block w-100 img-fluid" alt="Responsive image"/>
                      </div>
                    </div>
                      <a className="carousel-control-prev" href="#galeria" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="sr-only">Previous</span>
                      </a>
                      <a className="carousel-control-next" href="#galeria" role="button" data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="sr-only">Next</span>
                      </a>
                    </div>
               
              </div>
                
              <div className="row images">
                  
                    <img src={image1} className="img-fluid" alt="Responsive image"/>
                 
                 
                    <img src={image2} className="img-fluid" alt="Responsive image"/>
              
                 
                    <img src={image3} className="img-fluid" alt="Responsive image"/>
              
                    <img src={image4} className="img-fluid" alt="Responsive image"/>
                 
              </div>

              <div className="row">
                  <div className="col-sm-6">
                    <img src={section6} className="img-fluid" alt="Responsive image"/>
                  </div>
                  <div className="col-sm-6 text" align="left">
                    <img src={logo} className="img-fluid" alt="Responsive image"/>
                    <br /><br />
                    <p>
                      Sincrology es el resultado de años de experiencia desarrollando y
                      seleccionando soluciones afines a cualquier personalidad y estilo. 
                    </p>
                    <p>
                      Visítanos en cualquier de nuestros showrooms en
                      <span className="bold"> Puebla/Zavaleta y CDMX/Altavista/Interlomas.</span>
                    </p>
                    <p>
                      <span className="bold">
                      Contamos con la capacidad para contribuir en todo tipo de
                      proyectos de decoración, poniendo a tu disposición una amplia
                      gama de muebles, tapetes, persianas, cortinas, lámparas,
                      artículos decorativos y una limitada variead de estilos.
                      </span>
                    </p>
                    <p>
                      <span className="bold">
                      Nuestro objetivo es brindar un servicio que te acompaña en cada
                      paso en la creación de ambientes que solucionen tus
                      necesidades, dando a tus espacios un estilo único y personal.
                      </span>
                    </p>
                  </div>

              </div>
          </div>
        </div>
		);
	}
}

export default Section6;



