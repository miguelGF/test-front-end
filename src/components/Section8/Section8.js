import React, { Component } from 'react';
import './Section8.css';

class Section8 extends Component {
	render() {
		return (
			<div className="section8 jumbotron jumbotron-fluid">
	          <div className="container show" align="center">
	           <br /><br />
	           <div className="row show" align="center">
		            <h1><span className="showroom">SHOWROOMS</span></h1>
		            <br /><br /><br />
		        </div>
		        <div className="row show" align="center">
		            <button type="button" className="btn btn-outline-light">
	                      <span className="botones">ALTAVISTA ></span>
	                </button>
		            <button type="button" className="btn btn-outline-light">
	                      <span className="botones">INTERLOMAS ></span>
	                </button>
		            <button type="button" className="btn btn-outline-light">
	                      <span className="botones">ZAVALETA ></span>
	                </button>
	            </div>
	          </div>
	        </div>
		);
	}
}

export default Section8;