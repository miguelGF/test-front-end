import React, { Component } from 'react';
import './Section4.css';

import section4 from './section4.png';
import image1 from './sillon.png';
import image2 from './silla.png';
import image3 from './lamp.png';
import image4 from './cojin.png';



class Section4 extends Component {
	render() {
		return (
      <div className="section4 jumbotron jumbotron-fluid">
          <div className="container">
              <div className="row">
                <div className="col-sm-6 ambiente">
                  <div className="sub-ambiente">
                    <h1><span className="titulo-ambiente">Ambientes</span></h1>
                    <button type="button" className="btn-ambiente btn btn-outline-light">
                      <span className="botones boton-ambiente">VER MÁS ></span>
                    </button>
                  </div>
                </div>
                <div className="col-sm-6">
                  <div className="row images1">
                    <img src={image1} className="image-fluid" alt="Responsive image" />
                    <img src={image2} className="image-fluid" alt="Responsive image" />  
                  </div>

                  <div className="row division">
                  </div>

                  <div className="row">
                    <img src={image3} className="image-fluid" alt="Responsive image" />
                    <img src={image4} className="image-fluid" alt="Responsive image" />
                  </div>
                </div>
                <div className="btn-am2">
                    <button type="button" className="btn-ambiente2 btn btn-outline-light">
                          <span className="botones boton-ambiente">VER MÁS ></span>
                    </button>
                </div>
              </div>
          </div>
      </div>
		);
	}
}

export default Section4;



