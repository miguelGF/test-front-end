import React, { Component } from 'react';

import './NavBar.css';


class NavBar extends Component {
	render() {
		return (
			<nav className="navbar navbar-expand-lg navBar">

			  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="" aria-controls="" aria-expanded="false" aria-label="Toggle navigation">
			    <i className="material-icons menu">
					menu
			    </i>
			  </button>
			  <a className="navbar-brand" href="#">
			    <img src="images/sincrology-logo-invert.png"className="logo" />
			  </a>
			  <div className="menu2">
			    <form className="form-inline my-2 my-lg-0">
			        <a href="#">
				      	<i className="material-icons">
						   person
					    </i>
    				</a>
				    <a href="#">
					    <i className="material-icons">
						   shopping_cart
					    </i>
				    </a>
			    </form>
			  </div>
			  <div className="collapse navbar-collapse">
			    <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
			      <li className="nav-item active">
			        <a className="nav-link" href="#"><span className="main-link">COMPRAR POR AMBIENTES</span></a>
			      </li>
			      <li className="nav-item">
			        <a className="nav-link" href="#"><span className="main-link">SUCURSALES</span></a>
			      </li>
			      <li className="nav-item">
			        <a className="nav-link" href="#"><span className="main-link">LOYALTY PROGRAM</span></a>
			      </li>
			    </ul>
			    <form className="form-inline my-2 my-lg-0">
			        <a href="#">			
				        <i class="material-icons">
						   search
					    </i>
                    </a>
					<a href="#">
				      	<i class="material-icons">
						   person
					    </i>
    				</a>
				    <a href="#">
					    <i class="material-icons">
						   shopping_cart
					    </i>
				    </a>
			    </form>
			  </div>
			</nav>

		);
	}
}

export default NavBar;