import React, { Component } from 'react';
import './SubBar.css';


class SubBar extends Component {
	render() {
		return (
			<nav className="navbar navbar-expand-lg subBar">
			  <div className="collapse navbar-collapse subBar" id="navbarSupportedContent">
			    <ul className="navbar-nav mr-auto">
			      <li className="nav-item active">
			        <a className="nav-link" href="#"><span className="link-sub">SALA</span></a>
			      </li>
			      <li className="nav-item">
			        <a className="nav-link" href="#"><span className="link-sub">COMEDOR</span></a>
			      </li>
			      <li className="nav-item">
			        <a className="nav-link" href="#"><span className="link-sub">RECAMARA</span></a>
			      </li>
			      <li className="nav-item">
			        <a className="nav-link" href="#"><span className="link-sub">TAPETES</span></a>
			      </li>
			      <li className="nav-item">
			        <a className="nav-link" href="#"><span className="link-sub">EXTERIOR</span></a>
			      </li>
			    </ul>
			  </div>
			</nav>
		);
	}
}

export default SubBar;