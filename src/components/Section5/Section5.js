import React, { Component } from 'react';
import './Section5.css';

import libro from './libro.png';


class Section5 extends Component {
	render() {
		return (
      <div className="section5 jumbotron jumbotron-fluid">
          <div className="container" align="">
              <div className="row justify-content-md-center">
                  <div className="col">
                        <h1><span className="estilo">ESTILO</span></h1>
                        <h1><span className="escandinavo">ESCANDINAVO</span></h1>
                        <br /><br /><br />
                        <div className="botones-grupo">
                            <button type="button" className="btn btn-outline-light">
                              <span className="botones">EXPLORAR ></span>
                            </button>
                            <br /><br />
                            <button type="button" className="btn btn-outline-light">
                              <span className="botones">DESCARGAR ></span>
                            </button>
                            <br /><br />
                            <button type="button" className="btn btn-outline-light">
                              <span className="botones">COMPRA POR AMBIENTEA ></span>
                            </button>
                        </div>
                        <div className="imagen-estilo">
                          <img src={libro} className="img-fluid" alt="Responsive image" />
                        </div>
                    </div>
                  </div>
                  <div className="col">
                    <div className="imagen-estilo2">
                          <img src={libro} className="img-fluid" alt="Responsive image" />
                    </div>
                    <div className="botones-grupo2">
                            <button type="button" className="btn btn-outline-light">
                              <span className="botones">EXPLORAR ></span>
                            </button>
                            <br /><br />
                            <button type="button" className="btn btn-outline-light">
                              <span className="botones">DESCARGAR ></span>
                            </button>
                            <br /><br />
                            <button type="button" className="btn btn-outline-light">
                              <span className="botones">COMPRA POR AMBIENTEA ></span>
                            </button>
                  </div>

              </div>
          </div>
        </div>
		);
	}
}

export default Section5;



