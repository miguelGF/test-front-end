import React, { Component } from 'react';
import './Footer.css';

import safe from './safe.png';

class Footer extends Component {
	render() {
		return (
			<div className="footer">
	          <div className="container" align="left">
	              <div className="row">
	              	<div className="col-sm-3 company">
	              		<h6>COMPAÑIA</h6>
	              		<p><a href="#">Acerca de Nosotros</a></p>
	              		<p><a href="#">Blog</a></p>
	              	</div>
					<div className="col-sm-3 contact">
						<h6>CONTACTO</h6>
						<p><a href="#">Localizador de Tienda</a></p>
					</div>
					<div className="col-sm-3 follow">
						<h6>SÍGUENOS</h6>
						<a href="#" className="fa fa-facebook"></a>
						<a href="#" className="fa fa-instagram"></a>
						<a href="#" className="fa fa-pinterest"></a>
	              	</div>
					<div className="col-sm-3 terms">
						<h6>POLÍTICAS Y TÉRMINOS</h6>
						<p><a href="#">Localizador de Tienda</a></p>
					</div>
	              </div>
	            
	              <div className="row foot">
	                <div className="col-sm-12 line-footer">
	                	<hr />
	                </div>
	              	<div className="col-sm-4 copyright">
	              		Copyright &copy; 2019 Sincrology | Todos los derechos reservados
	              	</div>
	              	<div className="col-sm-12 line-footer2">
	                	<hr />
	                </div>
	              	<div className="col-sm-5">
	              	</div>
	              	<div className="col-sm-3 image">
	              		<img src={safe} className="img-fluid" alt="Responsive image"/>
	              	</div>
	              </div>
	          </div>
		    </div>
		);
	}
}

export default Footer;