import React, { Component } from 'react';
import './Newsletter.css';

class Newsletter extends Component {
	render() {
		return (
			<div className="newsletter">
	            <div className="container" align="center">
		            <p>
		            	Sé de los primeros en acceder a la información exclusiva
		            	sobre las últimas colecciones y descuentos
		            </p>
		            <div className="row">
		            	<div className="col-sm-2">
		            	</div>
		            	<div className="col-sm-4" align="right">
		            		<div className="form-group">		    
							    <input type="email" required className="form-control email"
							     placeholder="Ingresa tu dirección de correo electrónico" />
							</div>
		            	</div>
		            	<div className="col-sm-4 suscribe" align="left">
		            		<button type="button" className="btn btn-dark">SUSCRÍBETE AL NEWSLETTER ></button>
		            	</div>
		            	<div className="col-sm-2">
		            	</div>
		            </div>
		            <div className="row">
		            	<div className="col check">
		            		<div className="custom-control custom-checkbox">
		  						<input type="checkbox" className="custom-control-input" id="customCheck1" />
		  						<label className="custom-control-label" for="customCheck1">
		  							He leído y acepto los términos y condiciones relativos al tratamiento
		  							de mis datos personales
		  						</label>
							</div>
		            	</div>
		            </div>
	            </div>
	        </div>
		);
	}
}

export default Newsletter;